

import os

import pytest

import phpenv

def test_get_php_src_url():
    assert phpenv.get_php_src_url('5.4.12') == 'http://museum.php.net/php5/php-5.4.12.tar.gz'
    assert phpenv.get_php_src_url('5.5.15') == 'http://php.net/get/php-5.5.15.tar.gz/from/this/mirror'
