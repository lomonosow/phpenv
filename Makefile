.PHONY: default tests

SHELL:=/bin/bash

pre-release:
	@read -p "Enter next release version: " version; \
		git checkout -b release-$$version;
	@echo "============================================"
	@echo "== New release branch created             =="
	@echo "============================================"

release:
	@version=`git status | head -n 1 | awk '{print $$3}' | awk -F'-' '{print $$2}'`; \
		git checkout master &&\
		git merge --no-ff release-$$version &&\
		git tag -a $$version &&\
		git checkout develop &&\
		git merge --no-ff release-$$version
	
	@echo "============================================"
	@echo "== New release commited, push to repo     =="
	@echo "============================================"

default:
	: do nothing

test1:
	@echo "============================================"
	@echo "== test1: create new php5 environment     =="
	@echo "============================================"
	@rm -Rf env &&\
		./phpenv.py env --php 5 &&\
		. env/bin/activate &&\
		php --version

test2:
	@echo "============================================"
	@echo "== test2: create new php7 environment     =="
	@echo "============================================"
	@rm -Rf env &&\
		./phpenv.py env --php 7 &&\
		. env/bin/activate &&\
		php --version


test3:
	@echo "============================================"
	@echo "== test3: use virtualenv php5 environment =="
	@echo "============================================"
	@rm -Rf env &&\
		virtualenv --no-site-packages env &&\
		. env/bin/activate &&\
		./phpenv.py -p --php 5 &&\
		php --version

test4:
	@echo "============================================"
	@echo "== test4: use virtualenv php7 environment =="
	@echo "============================================"
	@rm -Rf env &&\
		virtualenv --no-site-packages env &&\
		. env/bin/activate &&\
		./phpenv.py -p --php 7 &&\
		php --version

test5:
	@echo "============================================"
	@echo "== test5: create new php5 environment     =="
	@echo "== Specific version 5.5.12                =="
	@echo "============================================"
	@rm -Rf env &&\
		./phpenv.py env --php 5.5.12 &&\
		. env/bin/activate &&\
		php --version

test6:
	@echo "============================================"
	@echo "== test6: create new php7 environment     =="
	@echo "== Specific version 7.0.2                =="
	@echo "============================================"
	@rm -Rf env &&\
		./phpenv.py env --php 7.0.2 &&\
		. env/bin/activate &&\
		php --version


tests: test1 test2 test3 test4 test5 test6
